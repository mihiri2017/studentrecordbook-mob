// Ionic template App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'SimpleRESTIonic' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('SimpleRESTIonic', ['ionic', 'backand', 'SimpleRESTIonic.controllers', 'SimpleRESTIonic.services'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }
        });
    })
    .config(function (BackandProvider, $stateProvider, $urlRouterProvider, $httpProvider) {

        BackandProvider.setAppName('photo'); // change here to your app name
        BackandProvider.setSignUpToken('35ef0d0d-8a5a-4fd0-a8dc-226b3efb73a4'); //token that enable sign up. see http://docs.backand.com/en/latest/apidocs/security/index.html#sign-up
        /*BackandProvider.setAnonymousToken('87c37623-a2d2-42af-93df-addc65c6e9ad');*/ // token is for anonymous login. see http://docs.backand.com/en/latest/apidocs/security/index.html#anonymous-access

        $stateProvider
            // setup an abstract state for the tabs directive
            .state('tab', {
                url: '/tabs',
                abstract: true,
                templateUrl: 'templates/tabs.html'
            })
            .state('tab.dashboard', {
                url: '/dashboard',
                views: {
                    'tab-dashboard': {
                        templateUrl: 'templates/tab-dashboard.html',
                        controller: 'DashboardCtrl as vm'
                    }
                }
            })
            .state('tab.login', {
                url: '/login',
                views: {
                    'tab-login': {
                        templateUrl: 'templates/tab-login.html',
                        controller: 'LoginCtrl as login'
                    }
                }
            });

        $urlRouterProvider.otherwise('/tabs/login');

        $httpProvider.interceptors.push('APIInterceptor');


    })

    .run(function ($rootScope, $state, LoginService, Backand) {

        function unauthorized() {
            console.log("user is unauthorized, sending to login");
            $state.go('tab.login');
        }

        function signout() {
            LoginService.signout();
        }

        $rootScope.$on('unauthorized', function () {
            unauthorized();
        });

        $rootScope.$on('$stateChangeSuccess', function (event, toState) {
            if (toState.name == 'tab.login') {
                signout();
            }
            else if (toState.name != 'tab.login' && Backand.getToken() === undefined) {
                unauthorized();
            }
        });

        //$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        //    //  if (fromState.name == 'tab.dashboard' && toState.name == 'tab.login' &&  !$rootScope.isCreating) {
        //    if (fromState.name == 'tab.dashboard' && toState.name == 'tab.login') {
        //        event.preventDefault();
        //        //$state.go('login');
        //        // alert('statechangestart : ' + $rootScope.isCreating);
        //        //if($rootScope.isCreating == false){
        //        //    ionic.Platform.exitApp();
        //        //}
        //
        //        if (confirm("Exit Application ?") == true) {
        //           // x = "You pressed OK!";
        //            ionic.Platform.exitApp();
        //        } else {
        //           // x = "You pressed Cancel!";
        //        }
        //
        //    }
        //    // debugger;
        //});

    })

